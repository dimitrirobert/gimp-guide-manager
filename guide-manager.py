#!/usr/bin/env python
# -*- coding: utf-8 -*-
#

from gimpfu import *

import os, sys, locale, gettext
import pprint
import gtk, gimp, gimpui

fi = __file__

# Localisation
APP_NAME = "guide-manager"

# initialize internationalization in a user-locale
locale_directory = os.path.join(os.path.dirname(os.path.abspath(fi)), 'locale')
gettext.install( APP_NAME, locale_directory, unicode=True )


class Main(gtk.Window):
    unit_list = gtk.combo_box_new_text()
    unit_list.append_text("px")
    unit_list.append_text("%")
    unit_list.set_active(0)
    
    unit_line = "px"
    unit_column = "px"
    
    margin_left = 0
    margin_right = 0
    margin_top = 0
    margin_bottom = 0
    
    columns_nb = 1
    column_width = 0
    column_width_remainder = 0
    vert_alley = 0
    
    lines_nb = 1
    line_height = 0
    line_height_remainder = 0
    horiz_alley = 0
    
    info_lines = _("Line's height {0}px - Remainder {1}px")
    info_columns = _("Column's width {0}px - Remainder {1}px")
    
    DEF_PAD = 5
    REF_LINE_MARGIN = 1
    REF_LINE_IMAGE = 0
    ref_line = REF_LINE_MARGIN
    REF_COLUMN_MARGIN = 1
    REF_COLUMN_IMAGE = 0
    ref_column = REF_COLUMN_MARGIN

    def __init__ (self):
        window = super(Main, self).__init__()
        self.set_title(_("Guides manager"))
#        self.set_default_size(570, 400)
        self.set_position(gtk.WIN_POS_CENTER)
        
        # Structure globale
        eg_cont = gtk.Table(2, 2)
        eg_left_vb = gtk.VBox()
        eg_right_vb = gtk.VBox()
        eg_cont.attach(eg_left_vb, 0, 1, 0, 1, xpadding=self.DEF_PAD, ypadding=self.DEF_PAD)
        eg_cont.attach(eg_right_vb, 1, 2, 0, 1, xpadding=self.DEF_PAD, ypadding=self.DEF_PAD)
        
        # est-ce que ça sert ?
        adjustment = gtk.Adjustment(value=1, lower=1, upper=500, step_incr=1, page_incr=1, page_size=1)
        
        # Colonne "lignes"
        # Nombre et gouttière
        eg_lines_frame1 = gtk.Frame(_("Lines"))
        eg_left_vb.pack_start(eg_lines_frame1, padding=self.DEF_PAD)
        
        eg_lines_tab1 = gtk.Table(3, 3)
        eg_lines_frame1.add(eg_lines_tab1)
        
        eg_lines_label = gtk.Label(_('Number'))
        eg_lines_number_value = gtk.SpinButton(gtk.Adjustment(value=1, lower=1, upper=512, step_incr=1), self.lines_nb)
        eg_lines_tab1.attach(eg_lines_label, 0, 1, 0, 1)
        eg_lines_tab1.attach(eg_lines_number_value, 1, 2, 0, 1)
        
        eg_lines_label = gtk.Label(_('Alley'))
        eg_lines_alley_value = gtk.SpinButton(gtk.Adjustment(value=0, lower=0, upper=128, step_incr=1), self.horiz_alley)
        eg_lines_alley_unit = gtk.Label(_('Pixels'))
        eg_lines_tab1.attach(eg_lines_label, 0, 1, 1, 2)
        eg_lines_tab1.attach(eg_lines_alley_value, 1, 2, 1, 2)
        eg_lines_tab1.attach(eg_lines_alley_unit, 2, 3, 1, 2)
        
        # events
        eg_lines_number_value.connect("value-changed", self.guides_change, "LINES_NB")
        eg_lines_alley_value.connect("value-changed", self.guides_change, "HALLEY")

        # info label
        self.eg_lines_label = gtk.Label(self.info_lines)
        eg_lines_tab1.attach(self.eg_lines_label, 0, 3, 2, 3)
        
        # Par rapport à
        eg_lines_frame2 = gtk.Frame(_("Relative to "))
        eg_left_vb.pack_start(eg_lines_frame2, padding=self.DEF_PAD)
        
        eg_lines_vb2 = gtk.VBox()
        eg_lines_frame2.add(eg_lines_vb2)
        eg_lines_relative = gtk.RadioButton(None, _("Margins"))
        eg_lines_relative.connect("toggled", self.ref_toggle, "LINE", self.REF_LINE_MARGIN)
        eg_lines_vb2.pack_start(eg_lines_relative, padding=self.DEF_PAD);
        eg_lines_relative = gtk.RadioButton(eg_lines_relative, _("Image"))
        eg_lines_relative.connect("toggled", self.ref_toggle, "LINE", self.REF_LINE_IMAGE)
        eg_lines_vb2.pack_start(eg_lines_relative, padding=self.DEF_PAD);
        
        # Marges
        eg_lines_frame3 = gtk.Frame(_("Margins"))
        eg_left_vb.pack_start(eg_lines_frame3, padding=self.DEF_PAD)
        
        eg_lines_tab3 = gtk.Table(2, 4)
        eg_lines_frame3.add(eg_lines_tab3)
        eg_lines_tab3.attach(gtk.Label(_('Top')), 0, 1, 0, 1)
        eg_lines_tab3.attach(gtk.Label(_('Bottom')), 0, 1, 1, 2)
        
        self.eg_lines_margin_top = gtk.SpinButton(gtk.Adjustment(value=0, lower=0, upper=1024, step_incr=1), self.margin_top)
        self.eg_lines_margin_bottom = gtk.SpinButton(gtk.Adjustment(value=0, lower=0, upper=1024, step_incr=1), self.margin_bottom)
        eg_lines_tab3.attach(self.eg_lines_margin_top, 1, 2, 0, 1)
        eg_lines_tab3.attach(self.eg_lines_margin_bottom, 1, 2, 1, 2)
        
        eg_lines_chain = gimpui.ChainButton(3)
        eg_lines_chain.set_active(True) 
        eg_lines_tab3.attach(eg_lines_chain, 2, 3, 0, 2)
        
        eg_lines_unit_list = gtk.combo_box_new_text()
        eg_lines_unit_list.append_text("px")
        eg_lines_unit_list.append_text("%")
        eg_lines_unit_list.set_active(0)
        eg_lines_tab3.attach(eg_lines_unit_list, 3, 4, 1, 2)
 
		# Events
        self.eg_lines_margin_top.connect("value-changed", self.margins_change, eg_lines_chain, self.eg_lines_margin_bottom, "TOP")
        self.eg_lines_margin_bottom.connect("value-changed", self.margins_change, eg_lines_chain, self.eg_lines_margin_top, "BOTTOM")
        eg_lines_unit_list.connect("changed", self.margins_unit_change, self.eg_lines_margin_top, self.eg_lines_margin_bottom, "LINE", eg_lines_chain)
      
       
        # Colonne "colonnes"
        # Nombre et gouttière
        eg_columns_frame1 = gtk.Frame(_("Columns"))
        eg_right_vb.pack_start(eg_columns_frame1, padding=self.DEF_PAD)
        
        eg_columns_tab1 = gtk.Table(3, 3)
        eg_columns_frame1.add(eg_columns_tab1)
        
        eg_columns_label = gtk.Label(_('Number'))
        eg_columns_number_value = gtk.SpinButton(gtk.Adjustment(value=1, lower=1, upper=512, step_incr=1), self.columns_nb)
        eg_columns_tab1.attach(eg_columns_label, 0, 1, 0, 1)
        eg_columns_tab1.attach(eg_columns_number_value, 1, 2, 0, 1)
        
        eg_columns_label = gtk.Label(_('Alley'))
        eg_columns_alley_value = gtk.SpinButton(gtk.Adjustment(value=0, lower=0, upper=128, step_incr=1), self.vert_alley)
        eg_columns_alley_unit = gtk.Label(_('Pixels'))
        eg_columns_tab1.attach(eg_columns_label, 0, 1, 1, 2)
        eg_columns_tab1.attach(eg_columns_alley_value, 1, 2, 1, 2)
        eg_columns_tab1.attach(eg_columns_alley_unit, 2, 3, 1, 2)
        
        # events
        eg_columns_number_value.connect("value-changed", self.guides_change, "COLUMNS_NB")
        eg_columns_alley_value.connect("value-changed", self.guides_change, "VALLEY")
        
        # info label
        self.eg_columns_label = gtk.Label(self.info_columns)
        eg_columns_tab1.attach(self.eg_columns_label, 0, 3, 2, 3)
        
        # Par rapport à
        eg_columns_frame2 = gtk.Frame(_("Relative to "))
        eg_right_vb.pack_start(eg_columns_frame2, padding=self.DEF_PAD)
        
        eg_columns_vb2 = gtk.VBox()
        eg_columns_frame2.add(eg_columns_vb2)
        eg_columns_relative = gtk.RadioButton(None, _("Margins"))
        eg_columns_relative.connect("toggled", self.ref_toggle, "COLUMN", self.REF_COLUMN_MARGIN)
        eg_columns_vb2.pack_start(eg_columns_relative, padding=self.DEF_PAD);
        eg_columns_relative = gtk.RadioButton(eg_columns_relative, _("Image"))
        eg_columns_relative.connect("toggled", self.ref_toggle, "COLUMN", self.REF_COLUMN_IMAGE)
        eg_columns_vb2.pack_start(eg_columns_relative, padding=self.DEF_PAD);
        
        # Marges
        eg_columns_frame3 = gtk.Frame(_("Margins"))
        eg_right_vb.pack_start(eg_columns_frame3, padding=self.DEF_PAD)
        
        eg_columns_tab3 = gtk.Table(2, 4)
        eg_columns_frame3.add(eg_columns_tab3)
        eg_columns_tab3.attach(gtk.Label(_('Left')), 0, 1, 0, 1)
        eg_columns_tab3.attach(gtk.Label(_('Right')), 0, 1, 1, 2)
        
        self.eg_columns_margin_left = gtk.SpinButton(gtk.Adjustment(value=0, lower=0, upper=1024, step_incr=1), self.margin_left)
        self.eg_columns_margin_right = gtk.SpinButton(gtk.Adjustment(value=0, lower=0, upper=1024, step_incr=1), self.margin_right)
        eg_columns_tab3.attach(self.eg_columns_margin_left, 1, 2, 0, 1)
        eg_columns_tab3.attach(self.eg_columns_margin_right, 1, 2, 1, 2)
        
        eg_columns_chain = gimpui.ChainButton(3)
        eg_columns_chain.set_active(True)
        eg_columns_tab3.attach(eg_columns_chain, 2, 3, 0, 2)
        
        eg_columns_unit_list = gtk.combo_box_new_text()
        eg_columns_unit_list.append_text("px")
        eg_columns_unit_list.append_text("%")
        eg_columns_unit_list.set_active(0)
        eg_columns_tab3.attach(eg_columns_unit_list, 3, 4, 1, 2)

 		# events
        self.eg_columns_margin_left.connect("value-changed", self.margins_change, eg_columns_chain, self.eg_columns_margin_right, "LEFT")
        self.eg_columns_margin_right.connect("value-changed", self.margins_change, eg_columns_chain, self.eg_columns_margin_left, "RIGHT")
        eg_columns_unit_list.connect("changed", self.margins_unit_change, self.eg_columns_margin_left, self.eg_columns_margin_right, "COLUMN", eg_columns_chain)
        
        # status line
        self.eg_statusline = gtk.Label()
        eg_cont.attach(self.eg_statusline, 0, 2, 1, 2)
        
        self.add(eg_cont)
        self.show_all()

#        self.refresh_guides()
        self.refresh_infos()
        
    def ref_toggle(self, widget, side, ref_value):
		if (side == "LINE"):
			self.ref_line = ref_value
		else:
			self.ref_column = ref_value
		self.refresh_guides()
		self.refresh_infos()
	
    def guides_change(self, spin_changed, data):
		if (data == "LINES_NB"):
			self.lines_nb = spin_changed.get_value()
		elif (data == "COLUMNS_NB"):
			self.columns_nb = spin_changed.get_value()
		elif (data == "HALLEY"):
			self.horiz_alley = spin_changed.get_value()
		elif (data == "VALLEY"):
			self.vert_alley = spin_changed.get_value()
			
		self.refresh_guides()
		self.refresh_infos()
	
    def set_spin_max_value(self, spin, maxval):
		adjustment = spin.get_adjustment()
		adjustment.upper = maxval
		spin.set_adjustment(adjustment)
		
		
    def margins_change(self, spin_changed, chain, spin_notchanged, value_changed):
		if (chain.get_active()):
			spin_notchanged.set_value(spin_changed.get_value())
		
		# Attention, les valeurs de marge sont toujours exprimées en pixels !
		# Seules les valeurs des spinbuttons peuvent être exprimées en pourcents
		
		# Copie des valeurs actuelles
		top = self.margin_top
		bottom = self.margin_bottom
		left = self.margin_left
		right = self.margin_right
		
		if (value_changed == "TOP"):
			top = spin_changed.get_value()
			bottom = spin_notchanged.get_value()
		elif (value_changed == "BOTTOM"):
			top = spin_notchanged.get_value()
			bottom = spin_changed.get_value()
		elif (value_changed == "LEFT"):
			left = spin_changed.get_value()
			right = spin_notchanged.get_value()
		elif (value_changed == "RIGHT"):
			left = spin_notchanged.get_value()
			right = spin_changed.get_value()
		
		# Calcul des valeurs max des spinbutton avant application du pourcentage	
		maxtop = self.image.height - bottom
		maxbottom = self.image.height - top
		maxleft = self.image.width - right
		maxright = self.image.width - left
		
		if (self.unit_column == "%" and (value_changed == "LEFT" or value_changed == "RIGHT")):
			maxleft = 100 - right
			maxright = 100 - left
			left = (left * self.image.width) / 100
			right = (right * self.image.width) / 100
		if (self.unit_line == "%" and (value_changed == "TOP" or value_changed == "BOTTOM")):
			maxtop = 100 - bottom
			maxbottom = 100 - top
			top = (top * self.image.height) / 100
			bottom = (bottom * self.image.height) / 100

		print "max top ", maxtop, "top ", top, " --- ", "max left ", maxleft, "left ", left
		
		# Changer la valeur max des spinbutton
		self.set_spin_max_value(self.eg_lines_margin_top, maxtop)
		self.set_spin_max_value(self.eg_lines_margin_bottom, maxbottom)
		self.set_spin_max_value(self.eg_columns_margin_left, maxleft)
		self.set_spin_max_value(self.eg_columns_margin_right, maxright)
		
		self.margin_top = top   
		self.margin_bottom = bottom
		self.margin_left   = left  
		self.margin_right  = right 
                                
		self.refresh_guides()
		self.refresh_infos()
	
    def margins_unit_change(self, unit_list, lt_value, rb_value, side, chain):
		unit = unit_list.get_active_text()
		if (side == "LINE"):
			if (unit != self.unit_line):
				self.unit_line = unit
				self.set_spin_max_value(self.eg_lines_margin_top, self.image.height)
				self.set_spin_max_value(self.eg_lines_margin_bottom, self.image.height)
				if (unit == "%"):
					lt_value.set_value((lt_value.get_value() * 100) / self.image.height)
					if (chain.get_active() == False):
						rb_value.set_value((rb_value.get_value() * 100) / self.image.height)
					
				elif (unit == "px"):
					lt_value.set_value((lt_value.get_value() * self.image.height) / 100)
					if (chain.get_active() == False):
						rb_value.set_value((rb_value.get_value() * self.image.height) / 100)
		else:
			if (unit != self.unit_column):
				self.unit_column = unit
				self.set_spin_max_value(self.eg_columns_margin_left, self.image.width)
				self.set_spin_max_value(self.eg_columns_margin_right, self.image.width)
				if (unit == "%"):
					lt_value.set_value((lt_value.get_value() * 100) / self.image.width)
					if (chain.get_active() == False):
						rb_value.set_value((rb_value.get_value() * 100) / self.image.width)
				elif (unit == "px"):
					lt_value.set_value((lt_value.get_value() * self.image.width) / 100)
					if (chain.get_active() == False):
						rb_value.set_value((rb_value.get_value() * self.image.width) / 100)
					


    def refresh_infos(self):
		self.eg_lines_label.set_text(self.info_lines.format(self.line_height, self.line_height_remainder))
		self.eg_columns_label.set_text(self.info_columns.format(self.column_width, self.column_width_remainder))

    def refresh_guides(self):
		# effacement des guides existants
        next_guide = pdb.gimp_image_find_next_guide(self.image, 0)
        while next_guide > 0:
            pdb.gimp_image_delete_guide(self.image, next_guide)
            next_guide = pdb.gimp_image_find_next_guide(self.image, 0)
        
        # création des nouveaux guides de marges
        if (self.margin_left > 0):
            pdb.gimp_image_add_vguide(self.image, self.margin_left)
        if (self.margin_right > 0):
            pdb.gimp_image_add_vguide(self.image, self.image.width - self.margin_right)
        if (self.margin_top > 0):
            pdb.gimp_image_add_hguide(self.image, self.margin_top)
        if (self.margin_bottom > 0):
            pdb.gimp_image_add_hguide(self.image, self.image.height - self.margin_bottom)
        
        # line's height and column's width values
        effective_height = self.image.height - self.ref_line * (self.margin_top + self.margin_bottom) - self.horiz_alley * (self.lines_nb - 1)
        self.line_height = effective_height // self.lines_nb
        self.line_height_remainder = effective_height % self.lines_nb

        effective_width = self.image.width - self.ref_column * (self.margin_left + self.margin_right) - self.vert_alley * (self.columns_nb - 1)
        self.column_width = effective_width // self.columns_nb
        self.column_width_remainder = effective_width % self.columns_nb
        
#        print "largeur ", self.column_width, " hauteur ", self.line_height
#        print "nb lignes ", self.lines_nb, "nb col ", self.columns_nb
        
        # guides creation
        # lines
        if (self.lines_nb > 1):
			y = self.line_height + self.margin_top * self.ref_line
			while (y < self.image.height - self.margin_bottom * self.ref_line):
				pdb.gimp_image_add_hguide(self.image, y)
				if (self.horiz_alley > 0):
					y = y + self.horiz_alley
					if (y < self.image.height - self.margin_bottom * self.ref_line):
						pdb.gimp_image_add_hguide(self.image, y)
				y = y + self.line_height
				
		# columns
        if (self.columns_nb > 1):
			x = self.column_width + self.margin_left * self.ref_column
			while (x < self.image.width - self.margin_right * self.ref_column):
				pdb.gimp_image_add_vguide(self.image, x)
				if (self.vert_alley > 0):
					x = x + self.vert_alley
					if (x < self.image.width - self.margin_right * self.ref_column):
						pdb.gimp_image_add_vguide(self.image, x)
				x = x + self.column_width


#    def __del__(self):

def show_guide_manager(img, drawable):
    r = Main()
    r.image = img
    #r.eg_statusline.set_text(img.filename)
    r.eg_statusline.set_text(locale_directory)
    gtk.main()

# This is the plugin registration function
register(
    "python_fu_guide_manager",    
    _("Tool for managing easily multiple guides in an image"),
    _("GNU GPL v3 or later."),
    "Dimitri Robert",
    "Dimitri Robert",
    _("January 2016"),
    "<Image>/Image/Guides/"+_("Guide manager"),
    "",
    [
    ],  
    [],
    show_guide_manager,
)

main()

