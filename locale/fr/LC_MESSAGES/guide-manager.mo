��          �      <      �     �     �  &   �     �     �                    ,  %   1     W     ]     e     l     y  4        �  @  �  
   �       &        /     8     R     i     �     �  $   �     �     �     �     �     �  ?   �     %                     
                                                                        	        Alley Bottom Column's width {0}px - Remainder {1}px Columns GNU GPL v3 or later. Guide manager Guides manager January 2016 Left Line's height {0}px - Remainder {1}px Lines Margins Number Relative to  Right Tool for managing easily multiple guides in an image Top Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2016-01-08 18:38+0100
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
 Gouttière Bas Largeur de colonne {0}px - Reste {1}px Colonnes GNU GPL v3 ou ultérieure Gestionnaire de guides Gestionnaire de guides Janvier 2016 Gauche Hauteur de ligne {0}px - Reste {1}px Lignes Marges Nombre Par rapport à  Droite Outil pour gérer facilement de multiples guides dans une image Haut 